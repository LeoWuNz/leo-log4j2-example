package nz.co.gogonz;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.trace("TRACE");
        logger.debug("DEBUG");
        logger.info("INFO");
        logger.warn("WARN");
        logger.error("ERROR");
        logger.fatal("FATAL");

        logger.info(new HashMap<String, String>(){{
            put("name","Leo");
            put("company","GOGONZ");
            put("city","Auckland");
        }});

        logger.info(new Student(1234567890L, "Mike", 20));

    }
}